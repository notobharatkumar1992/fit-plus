package model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by admin on 05-08-2016.
 */
public class FitDayModel  implements Parcelable{
   public int status,dataflow,id,file_type,user_id,view;
  public  String message,created,modified,file_name,file_thumb,comment;

    public FitDayModel() {

    }

    protected FitDayModel(Parcel in) {
        status = in.readInt();
        dataflow = in.readInt();
        id = in.readInt();
        file_type = in.readInt();
        user_id = in.readInt();
        view = in.readInt();
        message = in.readString();
        created = in.readString();
        modified = in.readString();
        file_name = in.readString();
        file_thumb = in.readString();
        comment=in.readString();
    }

    public static final Creator<FitDayModel> CREATOR = new Creator<FitDayModel>() {
        @Override
        public FitDayModel createFromParcel(Parcel in) {
            return new FitDayModel(in);
        }

        @Override
        public FitDayModel[] newArray(int size) {
            return new FitDayModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(status);
        dest.writeInt(dataflow);
        dest.writeInt(id);
        dest.writeInt(file_type);
        dest.writeInt(user_id);
        dest.writeInt(view);
        dest.writeString(message);
        dest.writeString(created);
        dest.writeString(modified);
        dest.writeString(file_name);
        dest.writeString(file_thumb);
        dest.writeString(comment);
    }
}
