package model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by parul on 15/1/16.
 */
public class UserDataModel implements Parcelable {
    public String message, first_name, last_name, email, dob, str_Gender, password, nickname, created, modified, city_name, views, social_id;
    public int userId, status, datatflow;
    public String avtar, avtar_thumb,presentation_vedio, bank_account, certificates, country_id, state_id, city_id, post_code, sex_group, fat_status, type, token;
    public int is_login, is_social, is_verified,follower_count,profile_visibility;
    public double latitude, longitude;
    public int role_id;

    public UserDataModel() {
    }


    protected UserDataModel(Parcel in) {
        role_id=in.readInt();
        profile_visibility=in.readInt();
        follower_count=in.readInt();
        message = in.readString();
        first_name = in.readString();
        last_name = in.readString();
        email = in.readString();
        dob = in.readString();
        str_Gender = in.readString();
        password = in.readString();
        nickname = in.readString();
        created = in.readString();
        modified = in.readString();
        city_name = in.readString();
        views = in.readString();
        social_id = in.readString();
        userId = in.readInt();
        status = in.readInt();
        datatflow = in.readInt();
        avtar = in.readString();
        presentation_vedio = in.readString();
        bank_account = in.readString();
        certificates = in.readString();
        country_id = in.readString();
        state_id = in.readString();
        city_id = in.readString();
        post_code = in.readString();
        sex_group = in.readString();
        fat_status = in.readString();
        avtar_thumb=in.readString();
        type = in.readString();
        token = in.readString();
        is_login = in.readInt();
        is_social = in.readInt();
        is_verified = in.readInt();
        latitude = in.readDouble();
        longitude = in.readDouble();
    }

    public static final Creator<UserDataModel> CREATOR = new Creator<UserDataModel>() {
        @Override
        public UserDataModel createFromParcel(Parcel in) {
            return new UserDataModel(in);
        }

        @Override
        public UserDataModel[] newArray(int size) {
            return new UserDataModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(message);
        dest.writeString(first_name);
        dest.writeString(last_name);
        dest.writeString(email);
        dest.writeString(dob);
        dest.writeString(str_Gender);
        dest.writeString(password);
        dest.writeString(nickname);
        dest.writeString(created);
        dest.writeString(modified);
        dest.writeString(city_name);
        dest.writeString(views);
        dest.writeString(social_id);
        dest.writeInt(userId);
        dest.writeInt(status);
        dest.writeInt(datatflow);
        dest.writeString(avtar);
        dest.writeString(presentation_vedio);
        dest.writeString(bank_account);
        dest.writeString(certificates);
        dest.writeString(country_id);
        dest.writeString(state_id);
        dest.writeString(city_id);
        dest.writeString(post_code);
        dest.writeString(sex_group);
        dest.writeString(fat_status);
        dest.writeString(type);
        dest.writeString(token);
        dest.writeInt(is_login);
        dest.writeInt(is_social);
        dest.writeInt(is_verified);
        dest.writeDouble(latitude);
        dest.writeDouble(longitude);
        dest.writeInt(follower_count);
        dest.writeInt(profile_visibility);
        dest.writeInt(role_id);
        dest.writeString(avtar_thumb);
    }
}
