package fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import com.fitplus.AppDelegate;
import com.fitplus.R;
import com.fitplus.SurveyForm;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import Async.PostAsync;
import Constants.Parameters;
import Constants.ServerRequestConstants;
import Constants.Tags;
import interfaces.OnReciveServerResponse;
import model.BecomeCoachModel;
import model.PostAysnc_Model;
import utils.Prefs;

/**
 * Created by admin on 26-07-2016.
 */
public class FindCoach extends Fragment implements OnClickListener, OnReciveServerResponse {
    private View rootview;
    TextView findCoach, Become_Coach;
    private BecomeCoachModel become_coach;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.find, container, false);

        return rootview;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
    }

    private void initView(View view) {
        view.findViewById(R.id.find_coach).setOnClickListener(this);
        view.findViewById(R.id.be_coach).setOnClickListener(this);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.find_coach:

                break;
            case R.id.be_coach:
                execute_Become_Coach();
                break;

        }
    }

    private void execute_Become_Coach() {
        try {
            if (AppDelegate.haveNetworkConnection(getActivity(), true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                PostAsync mPostAsyncObj;
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE.trim());
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.user_id, new Prefs(getActivity()).getUserdata().userId);
                //  AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.device_token, AppDelegate.getValue(Signup.this, Tags.REGISTRATION_ID), ServerRequestConstants.Key_PostintValue);
                mPostAsyncObj = new PostAsync(getActivity(), FindCoach.this, ServerRequestConstants.BECOME_COACH,
                        mPostArrayList, FindCoach.this);
                AppDelegate.showProgressDialog(getActivity());
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
            AppDelegate.ShowDialog(getActivity(), getActivity().getResources().getString(R.string.try_again), "Alert!!!");
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        AppDelegate.hideProgressDialog(getActivity());
        if (!AppDelegate.isValidString(result)) {
            AppDelegate.ShowDialog(getActivity(), getActivity().getResources().getString(R.string.time_out), "Time out!!!");
            return;
        }
        if (apiName.equals(ServerRequestConstants.BECOME_COACH)) {
            parseBecome_coach(result);
        }
    }

    private void parseBecome_coach(String result) {
        try {
            JSONObject response = new JSONObject(result);
            response.getString("message");
            ArrayList<BecomeCoachModel> becomeCoachModels = new ArrayList<>();
            AppDelegate.ShowDialog(getActivity(), response.getString("message"), "");
            JSONArray jsonArray = response.getJSONArray("response");
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject obj = jsonArray.getJSONObject(i);
                become_coach = new BecomeCoachModel();
                become_coach.status = obj.getInt("status");
                become_coach.doc_status = obj.getInt("doc_status");
                become_coach.dataflow = obj.getInt("dataFlow");
                become_coach.id = obj.getInt("id");
                become_coach.question = obj.getString("question");
                become_coach.option1 = obj.getString("option1");
                become_coach.option2 = obj.getString("option2");
                become_coach.option3 = obj.getString("option3");
                become_coach.option4 = obj.getString("option4");
                become_coach.answer = obj.getString("answer");
                become_coach.question_status = obj.getInt("status");
                become_coach.answer_status=0;
                becomeCoachModels.add(become_coach);
            }
            Intent intent=new Intent(getActivity(), SurveyForm.class);
            Bundle bundle=new Bundle();
            bundle.putParcelableArrayList(Tags.parcel_becomeCoach,becomeCoachModels);
            intent.putExtras(bundle);
            startActivity(intent);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
