package fragments;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.camerademo.CameraActivity;
import com.fitplus.AppDelegate;
import com.fitplus.MainActivity;
import com.fitplus.R;
import com.fitplus.Trending;

import utils.NonSwipeableViewPager;

public class NavigationFragment extends Fragment implements View.OnClickListener {
    private NonSwipeableViewPager mViewPager;
    View rootview;
    private TabLayout tabLayout;

    private int[] tabIcons = {
            R.drawable.globe,
            R.drawable.news,
    };
    private FrameLayout frame;
    public FragmentManager fragmentManager;
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ImageView trending_link;
    private RelativeLayout toolbar;
    TextView toolbar_title;
    private ImageView tool_menu;
    ImageView img_news_indicator, img_globe_indicator;
    public static final int PANEL_GlOBE = 0, PANEL_NEWS = 1;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.navigation_fragment, container, false);
        initView();
        return rootview;
    }

    private void initView() {
        toolbar = (RelativeLayout) rootview.findViewById(R.id.toolbar);
        toolbar_title = (TextView) rootview.findViewById(R.id.title);
        tool_menu = (ImageView) rootview.findViewById(R.id.menu);
        tool_menu.setOnClickListener(this);
        rootview.findViewById(R.id.rl_globe).setOnClickListener(this);
        rootview.findViewById(R.id.rl_news).setOnClickListener(this);
        img_news_indicator = (ImageView) rootview.findViewById(R.id.img_news_indicator);
        img_globe_indicator = (ImageView) rootview.findViewById(R.id.img_globe_indicator);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupTabLayout();
    }

    private void setupTabLayout() {
        mSectionsPagerAdapter = new SectionsPagerAdapter(getChildFragmentManager());
        mViewPager = (NonSwipeableViewPager) rootview.findViewById(R.id.sub_container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        fragmentManager = getActivity().getSupportFragmentManager();
        mViewPager.setCurrentItem(0);
        setInitailToolBar(0);
    }

    public void setInitailToolBar(int value) {
        img_news_indicator.setSelected(false);
        img_globe_indicator.setSelected(false);

        switch (value) {
            case 0:
                img_globe_indicator.setSelected(true);
                break;
            case 1:
                img_news_indicator.setSelected(true);
                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.trending_link:
                Intent intent = new Intent(getActivity(), Trending.class);
                startActivity(intent);
                break;
            case R.id.menu:
                AppDelegate.LogT("menu clicked");
                new MainActivity().toggleSlider();
            case R.id.rl_globe:
                setInitailToolBar(0);
                mSectionsPagerAdapter.getItem(PANEL_GlOBE);
                break;
            case R.id.rl_news:
                setInitailToolBar(1);
                mSectionsPagerAdapter.getItem(PANEL_NEWS);
                break;
        }
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {
        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {

            switch (position) {
                case 0:
                    AppDelegate.LogT("position clicked" + position + "");
                    return new GlobalMap();
                case 1:
                    return new ListedMap();
            }
            return null;
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return null;
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }
}
