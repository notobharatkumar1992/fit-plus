package fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fitplus.AppDelegate;
import com.fitplus.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import Async.PostAsync;
import Constants.Parameters;
import Constants.ServerRequestConstants;
import Constants.Tags;
import interfaces.OnDialogClickListener;
import interfaces.OnReciveServerResponse;
import model.PostAysnc_Model;
import model.UserDataModel;
import utils.Prefs;

/**
 * Created by admin on 26-07-2016.
 */
public class FitTeam extends Fragment implements OnClickListener, AdapterView.OnItemClickListener, OnReciveServerResponse, OnDialogClickListener {
    private View rootview;
    RelativeLayout toolbar;
    TextView toolbar_title;
    ImageView tool_menu, plus;
    RecyclerView team_list;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.team_list, container, false);
        return rootview;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();
    }

    private void initView() {
        toolbar = (RelativeLayout) rootview.findViewById(R.id.toolbar);
        toolbar_title = (TextView) rootview.findViewById(R.id.title);
        tool_menu = (ImageView) rootview.findViewById(R.id.menu);
        tool_menu.setOnClickListener(this);
        plus = (ImageView) rootview.findViewById(R.id.plus);
        plus.setOnClickListener(this);
        team_list = (RecyclerView) rootview.findViewById(R.id.team_list);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
       // execute_teamList();
    }

    private void execute_teamList() {
        try {
            if (AppDelegate.haveNetworkConnection(getActivity(), true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.user_id, new Prefs(getActivity()).getUserdata().userId);
                PostAsync mPostAsyncObj;
                mPostAsyncObj = new PostAsync(getActivity(), FitTeam.this, ServerRequestConstants.TEAM_LIST,
                        mPostArrayList, null);
                AppDelegate.showProgressDialog(getActivity());
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.ShowDialog(getActivity(), "Please try again.", "Alert!!!");
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        switch (view.getId()) {
            case R.id.months:

                break;
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        AppDelegate.hideProgressDialog(getActivity());
        if (!AppDelegate.isValidString(result)) {
            AppDelegate.ShowDialog(getActivity(), getActivity().getResources().getString(R.string.time_out), "Time out!!!");
            return;
        }
        if (apiName.equals(ServerRequestConstants.TEAM_LIST)) {
            parseFitTeamList(result);
        }
    }

    private void parseFitTeamList(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getInt(Tags.status) == 1) {
                JSONObject response = jsonObject.getJSONObject(Tags.response);
                JSONArray jsonArray = response.getJSONArray(Tags.normal);
                JSONArray follower = response.getJSONArray(Tags.follower);
                ArrayList<UserDataModel> trending = new ArrayList<>();
                for (int i = 0; i < jsonArray.length(); i++) {
                    UserDataModel userDataModel = new UserDataModel();
                    JSONObject object = jsonArray.getJSONObject(i);
                    userDataModel.first_name = object.getString(Tags.first_name);
                    userDataModel.last_name = object.getString(Tags.last_name);
                    userDataModel.email = object.getString(Tags.email);
                    userDataModel.password = object.getString(Tags.password);
                    userDataModel.created = object.getString(Tags.created);
                    userDataModel.str_Gender = object.getString(Tags.gender);
                    userDataModel.userId = object.getInt(Tags.user_id);
                    userDataModel.dob = object.getString(Tags.birthdate);
                    userDataModel.nickname = object.getString(Tags.nick_name);
                    userDataModel.avtar = object.getString(Tags.avtar);
                    userDataModel.views = object.getString(Tags.views);
                    userDataModel.follower_count = object.getInt(Tags.follower_count);
                    userDataModel.profile_visibility = object.getInt(Tags.profile_visibility);
                    userDataModel.presentation_vedio = object.getString(Tags.presentation_vedio);
                    userDataModel.bank_account = object.getString(Tags.bank_account);
                    userDataModel.certificates = object.getString(Tags.certificates);
                    userDataModel.country_id = object.getString(Tags.country_id);
                    userDataModel.role_id = object.getInt(Tags.role_id);
                    userDataModel.state_id = object.getString(Tags.state_id);
                    userDataModel.city_id = object.getString(Tags.city_id);
                    userDataModel.post_code = object.getString(Tags.post_code);
                    userDataModel.sex_group = object.getString(Tags.sex_group);
                    userDataModel.fat_status = object.getString(Tags.fat_status);
                    userDataModel.type = object.getString(Tags.type);
                    userDataModel.token = object.getString(Tags.token);
                    userDataModel.is_login = object.getInt(Tags.is_login);
                    userDataModel.is_social = object.getInt(Tags.is_social);
                    userDataModel.is_verified = object.getInt(Tags.is_verified);
                    userDataModel.latitude = object.getDouble(Tags.latitude);
                    userDataModel.longitude = object.getDouble(Tags.longitude);
                    if (object.has(Tags.avtar_thumb)) {
                        userDataModel.avtar_thumb = object.getString(Tags.avtar_thumb);
                    }
                    trending.add(userDataModel);
                }
                // AppDelegate.ShowDialogID(getActivity(), jsonObject.getString(Tags.message), "Alert", Tags.ok, GlobalMap.this);
            } else {
                if(jsonObject.getInt(Tags.status) == 0){
                    AppDelegate.ShowDialogID(getActivity(), jsonObject.getString(Tags.message), "Alert", Tags.ok, FitTeam.this);
                }
              else  if (jsonObject.has(Tags.response) && jsonObject.optJSONObject(Tags.response) != null /*&& jsonObject.getJSONObject(Tags.response).has(Tags.suspand_status) && jsonObject.getJSONObject(Tags.response).optString(Tags.suspand_status) != null*/) {
                    AppDelegate.ShowDialogID(getActivity(), jsonObject.getString(Tags.message), "Alert", Tags.ok, FitTeam.this);
                }
            }
        } catch (Exception e) {
            AppDelegate.showAlert(getActivity(), getResources().getString(R.string.response_error));
            AppDelegate.LogE(e);
        }
    }

    @Override
    public void setOnDialogClickListener(String name) {

    }
}
