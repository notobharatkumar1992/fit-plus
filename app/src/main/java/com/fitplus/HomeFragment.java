package com.fitplus;
import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.camerademo.CameraActivity;
import java.io.File;

import fragments.CaptureVideo;
import fragments.FindCoach;
import fragments.FitTeam;
import fragments.GlobalMap;
import fragments.NavigationFragment;
import fragments.StatsFragment;
public class HomeFragment extends Fragment implements View.OnClickListener {
    private FrameLayout frameLayout;
    private int[] tabIcons = {
            R.drawable.user_active,
            R.drawable.camera,
            R.drawable.navigation,
            R.drawable.group
    };
    private TabLayout tabLayout;
    public static FragmentManager fragmentManager;
    public RelativeLayout toolbar;
    public TextView toolbar_title;
    public ImageView menu_icon, trendinglink;
    ImageView user, camera, map, group;
    public static final int PANEL_USER = 0, PANEL_CAMERA = 1, PANEL_MAP = 2, PANEL_GROUP = 3;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.home_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
    }

    private void initView(View rootview) {
        frameLayout = (FrameLayout) rootview.findViewById(R.id.container);
        menu_icon = (ImageView) rootview.findViewById(R.id.menu);
        trendinglink = (ImageView) rootview.findViewById(R.id.trending_link);
        user = (ImageView) rootview.findViewById(R.id.user);
        map = (ImageView) rootview.findViewById(R.id.map);
        camera = (ImageView) rootview.findViewById(R.id.camera);
        group = (ImageView) rootview.findViewById(R.id.group);
        rootview.findViewById(R.id.rl_camera).setOnClickListener(this);
        rootview.findViewById(R.id.rl_group).setOnClickListener(this);
        rootview.findViewById(R.id.rl_map).setOnClickListener(this);
        rootview.findViewById(R.id.rl_user).setOnClickListener(this);
        fragmentManager = getActivity().getSupportFragmentManager();
        setInitailToolBar(2);
        displayView(PANEL_MAP);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rl_user:
                if (!(getActivity().getSupportFragmentManager().findFragmentById(R.id.container) instanceof FindCoach))
                    displayView(PANEL_USER);
                AppDelegate.LogT("clicked tab PANEL_USER");
                break;
            case R.id.rl_camera:
                displayView(PANEL_CAMERA);
                return;
            case R.id.rl_map:
                if (!(getActivity().getSupportFragmentManager().findFragmentById(R.id.container) instanceof NavigationFragment))
                    displayView(PANEL_MAP);
                AppDelegate.LogT("clicked tab PANEL_MAP");
                break;
            case R.id.rl_group:
                if (!(getActivity().getSupportFragmentManager().findFragmentById(R.id.container) instanceof FitTeam))
                    displayView(PANEL_GROUP);
                AppDelegate.LogT("clicked tab PANEL_GROUP");
                break;
            default:
                break;
        }
    }

    private void displayView(int position) {
        AppDelegate.hideKeyBoard(getActivity());
        setInitailToolBar(position);
        Fragment fragment = null;
        switch (position) {
            case PANEL_USER:
                fragment = new FindCoach();
                break;
            case PANEL_CAMERA:
                fragment = new CaptureVideo();
                break;
            case PANEL_MAP:
                fragment = new NavigationFragment();
                break;
            case PANEL_GROUP:
                fragment = new FitTeam();
                break;
            default:
                break;
        }
        if (fragment != null) {
            AppDelegate.showFragmentAnimation(getActivity().getSupportFragmentManager(), fragment, R.id.container);
        } else if (position != 6) {
            AppDelegate.LogE("Error in creating fragment");
        }
    }

    public void setInitailToolBar(int value) {
        if (value == 1) {
            startActivityForResult(new Intent(getActivity(), CameraActivity.class), 500);
          //  setvideorecorder();
            return;
        }
        user.setSelected(false);
        camera.setSelected(false);
        map.setSelected(false);
        group.setSelected(false);
        switch (value) {
            case 0:
                user.setSelected(true);
                break;
            case 1:
                camera.setSelected(true);
                break;
            case 2:
                map.setSelected(true);
                break;
            case 3:
                group.setSelected(true);
                break;
        }
    }

    private void setvideorecorder() {
        File saveDir = null;
        saveDir = new File(Environment.getExternalStorageDirectory(), "MaterialCamera");
        saveDir.mkdirs();
    }
}
