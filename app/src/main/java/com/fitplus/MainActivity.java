package com.fitplus;

import android.content.Intent;
import android.content.IntentSender;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import Constants.Tags;
import fragments.GlobalMap;
import fragments.StatsFragment;
import utils.Prefs;
import utils.SlidingPaneLayout1;

public class MainActivity extends FragmentActivity implements View.OnClickListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    ImageView bar1, bar2, bar3, bar4, bar5, bar6, inbox_notification, calendar_notification;
    TextView fit_days, inbox, stats, calendar, setting;
    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private double currentLatitude = 0, currentLongitude = 0;
    public LinearLayout side_panel;
    public SlidingPaneLayout1 mSlidingPaneLayout;
    public SlideMenuClickListener menuClickListener = new SlideMenuClickListener();
    public static final int PANEL_FITDAYS = 0, PANEL_INBOX = 1, PANEL_STATS = 2, PANEL_CALENDAR = 3, PANEL_SETTINGS = 4, PANEL_LOGOUT = 5;
    public boolean isSlideOpen = false;
    public int ratio, ride_cancel_time = 30;
    public float init = 0.0f;
    private TextView username;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        initGPS();
        AppDelegate.showGPSAlert(this);
    }

    private void initGPS() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        AppDelegate.LogT("mGoogleApiClient Initialited");
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(500)        // 10 seconds, in milliseconds
                .setFastestInterval(500); // 10 second, in milliseconds
        AppDelegate.LogT("mLocationRequest Initialited");
    }

    private void initView() {
        mSlidingPaneLayout = (SlidingPaneLayout1) findViewById(R.id.mSlidingPaneLayout);
        mSlidingPaneLayout.setSliderFadeColor(getResources().getColor(android.R.color.transparent));
        mSlidingPaneLayout.setPanelSlideListener(new SliderListener());
        side_panel = (LinearLayout) findViewById(R.id.side_panel);
        bar1 = (ImageView) findViewById(R.id.bar1);
        bar2 = (ImageView) findViewById(R.id.bar2);
        bar3 = (ImageView) findViewById(R.id.bar3);
        bar4 = (ImageView) findViewById(R.id.bar4);
        bar5 = (ImageView) findViewById(R.id.bar5);
        bar6 = (ImageView) findViewById(R.id.bar6);
        username = (TextView) findViewById(R.id.username);
        findViewById(R.id.rl_inbox).setOnClickListener(this);
        findViewById(R.id.rl_fitdays).setOnClickListener(this);
        findViewById(R.id.rl_calendar).setOnClickListener(this);
        findViewById(R.id.rl_stats).setOnClickListener(this);
        findViewById(R.id.rl_setting).setOnClickListener(this);
        findViewById(R.id.rl_logout).setOnClickListener(this);
        ImageView background_img = (ImageView) findViewById(R.id.background_img);
//second parametre is radius
        background_img.setImageBitmap(AppDelegate.blurRenderScript(this, ((BitmapDrawable) getResources().getDrawable(R.drawable.download)).getBitmap()));
        AppDelegate.showFragmentAnimation(getSupportFragmentManager(), new HomeFragment(), R.id.main_content);
        AppDelegate.LogT("clicked tab");
        username.setText(new Prefs(this).getUserdata().first_name);
    }

    @Override
    protected void onPause() {
        super.onPause();
        //Disconnect from API onPause()
        try {
            if (mGoogleApiClient.isConnected()) {
                LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, (com.google.android.gms.location.LocationListener) MainActivity.this);
                mGoogleApiClient.disconnect();
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        AppDelegate.LogT("onConnected Initialited");
        if (location == null) {
            try {
                AppDelegate.LogT("onConnected Initialited== null");
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, (com.google.android.gms.location.LocationListener) MainActivity.this);
            } catch (Exception e) {
                AppDelegate.LogE(e);
            }
        } else {
            currentLatitude = location.getLatitude();
            currentLongitude = location.getLongitude();
            AppDelegate.LogT("latLng = " + currentLatitude + ", " + currentLongitude);
            new Prefs(this).putStringValue(Tags.TAG_LAT, String.valueOf(currentLatitude));
            new Prefs(this).putStringValue(Tags.TAG_LONG, String.valueOf(currentLongitude));
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            try {
                AppDelegate.LogT("onConnectionFailed Initialited" + connectionResult);
                connectionResult.startResolutionForResult(this, CONNECTION_FAILURE_RESOLUTION_REQUEST);

            } catch (IntentSender.SendIntentException e) {
                AppDelegate.LogE(e);
            }
        } else {
            Log.e("Error", "Location services connection failed with code " + connectionResult.getErrorCode());
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppDelegate.hideKeyBoard(this);
        mGoogleApiClient.connect();
    }

    @Override
    public void onLocationChanged(android.location.Location location) {
        currentLatitude = location.getLatitude();
        currentLongitude = location.getLongitude();
        AppDelegate.LogGP("onLocationChanged latLng = " + currentLatitude + ", " + currentLongitude);
        new Prefs(this).putStringValue(Tags.TAG_LAT, String.valueOf(currentLatitude));
        new Prefs(this).putStringValue(Tags.TAG_LONG, String.valueOf(currentLongitude));
        try {
            if (mGoogleApiClient.isConnected()) {
                LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, (com.google.android.gms.location.LocationListener) MainActivity.this);
                mGoogleApiClient.disconnect();
                AppDelegate.LogGP("Fused Location api disconnect called");
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onProviderDisabled(String provider) {
    }

    @SuppressWarnings("deprecation")
    public void setInitailSideBar(int value) {
        bar1.setVisibility(View.INVISIBLE);
        bar2.setVisibility(View.INVISIBLE);
        bar3.setVisibility(View.INVISIBLE);
        bar4.setVisibility(View.INVISIBLE);
        bar5.setVisibility(View.INVISIBLE);
        bar6.setVisibility(View.INVISIBLE);
        switch (value) {
            case 0:
                bar1.setVisibility(View.VISIBLE);
                break;
            case 1:
                bar2.setVisibility(View.VISIBLE);
                break;
            case 2:
                bar3.setVisibility(View.VISIBLE);
                break;
            case 3:
                bar4.setVisibility(View.VISIBLE);
                break;
            case 4:
                bar5.setVisibility(View.VISIBLE);
                break;
            case 5:
                bar6.setVisibility(View.VISIBLE);
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //  getMenuInflater().inflate(R.menu.menu_fit_radar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rl_inbox:
                toggleSlider();
                if (!(getSupportFragmentManager().findFragmentById(R.id.main_content) instanceof StatsFragment))
                    menuClickListener.onItemClick(null, v, PANEL_INBOX, PANEL_INBOX);
                AppDelegate.LogT("clicked tab");
                break;
            case R.id.rl_calendar:
                toggleSlider();
                if (!(getSupportFragmentManager().findFragmentById(R.id.main_content) instanceof StatsFragment))
                    menuClickListener.onItemClick(null, v, PANEL_CALENDAR, PANEL_CALENDAR);
                AppDelegate.LogT("clicked tab");
                break;
            case R.id.rl_stats:
                toggleSlider();
                if (!(getSupportFragmentManager().findFragmentById(R.id.main_content) instanceof StatsFragment))
                    menuClickListener.onItemClick(null, v, PANEL_STATS, PANEL_STATS);
                AppDelegate.LogT("clicked tab");
                break;
            case R.id.rl_fitdays:
                toggleSlider();
                if (!(getSupportFragmentManager().findFragmentById(R.id.main_content) instanceof HomeFragment))
                    menuClickListener.onItemClick(null, v, PANEL_FITDAYS, PANEL_FITDAYS);
                AppDelegate.LogT("clicked tab");
                break;
            case R.id.rl_setting:
                toggleSlider();
                if (!(getSupportFragmentManager().findFragmentById(R.id.main_content) instanceof StatsFragment))
                    menuClickListener.onItemClick(null, v, PANEL_SETTINGS, PANEL_SETTINGS);
                AppDelegate.LogT("clicked tab");
                break;

            case R.id.rl_logout:
                toggleSlider();
                new Prefs(this).clearTempPrefs();
                new Prefs(this).clearSharedPreference();
                startActivity(new Intent(MainActivity.this, Login.class));
                finish();
                break;
        }
    }


    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().findFragmentById(R.id.main_content) instanceof HomeFragment) {
            finish();
        } else {
            super.onBackPressed();
        }
    }

    private class SlideMenuClickListener implements
            ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view,
                                final int position, long id) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    displayView(position);
                }
            }, 600);
        }
    }

    private void displayView(int position) {
        AppDelegate.hideKeyBoard(MainActivity.this);
        setInitailSideBar(position);
        Fragment fragment = null;
        switch (position) {
            case PANEL_FITDAYS:
                fragment = new HomeFragment();
                break;
            case PANEL_INBOX:
                fragment = new GlobalMap();
                break;
            case PANEL_STATS:
                fragment = new StatsFragment();
                break;
            case PANEL_CALENDAR:
                fragment = new StatsFragment();
                break;
            case PANEL_SETTINGS:
                fragment = new StatsFragment();
                break;
            case PANEL_LOGOUT:
                startActivity(new Intent(MainActivity.this, Login.class));
                finish();
                break;
            default:
                break;
        }
        if (fragment != null) {
            AppDelegate.showFragmentAnimation(getSupportFragmentManager(), fragment, R.id.main_content);
        } else if (position != 6) {
            AppDelegate.LogE("Error in creating fragment");
        }
    }

    class SliderListener extends SlidingPaneLayout1.SimplePanelSlideListener {

        @Override
        public void onPanelOpened(View panel) {
            if (!isSlideOpen) {
                isSlideOpen = true;
            }
            AppDelegate.hideKeyBoard(MainActivity.this);
        }

        @Override
        public void onPanelClosed(View panel) {
            if (isSlideOpen) {
                isSlideOpen = false;
            }
        }

        @Override
        public void onPanelSlide(View view, float slideOffset) {
            ratio = (int) -(0 - (slideOffset) * 255);
            whileSlide(ratio);
        }
    }

    public void toggleSlider() {
        if (mSlidingPaneLayout != null)
            if (!mSlidingPaneLayout.isOpen()) {
                mSlidingPaneLayout.openPane();
            } else {
                mSlidingPaneLayout.closePane();
            }
    }

    public void whileSlide(int view) {
        int newalfa = 255 - view;
        float subvalue = newalfa / 2.55f;
        float f = (100f - subvalue) * 0.01f;
        Animation alphaAnimation = new AlphaAnimation(init, f);
        alphaAnimation.setDuration(0);
        alphaAnimation.setFillAfter(true);
        init = f;
        side_panel.startAnimation(alphaAnimation);
    }


}
