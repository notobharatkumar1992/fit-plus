package com.fitplus;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.crashlytics.android.Crashlytics;

import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import Async.PostAsync;
import Constants.Parameters;
import Constants.ServerRequestConstants;
import Constants.Tags;
import interfaces.OnDialogClickListener;
import interfaces.OnReciveServerResponse;
import io.fabric.sdk.android.Fabric;
import model.PostAysnc_Model;
import model.SignUpmodel;
import model.UserDataModel;
import utils.Prefs;

public class Signup extends AppCompatActivity implements View.OnClickListener, OnReciveServerResponse, OnDialogClickListener {


    private TextView register, birthday;
    EditText firstname, lastname, city, email, password, confirmpass, nickname;
    ToggleButton gender;
    String first_name, last_name, cityy, email_id, pass, confirm_pass, birth, Gender, nick_name;
    private int mYear, mMonth, mDay, mHour, mMinute;
    public static final String DATE_FORMAT = "dd-MM-yyyy";
    public static final String TIME_FORMAT_12_HOUR = "hh:mm aa";
    public static final String TIME_FORMAT_24_HOUR = "HH:mm";
    private Prefs prefs;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.signup);
        prefs = new Prefs(this);
        initView();
    }

    private void initView() {
        birthday = (TextView) findViewById(R.id.birthday);
        register = (TextView) findViewById(R.id.register);
        gender = (ToggleButton) findViewById(R.id.gender);
        firstname = (EditText) findViewById(R.id.first_name);
        lastname = (EditText) findViewById(R.id.last_name);
        city = (EditText) findViewById(R.id.city);
        email = (EditText) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.password);
        nickname = (EditText) findViewById(R.id.nick_name);
        confirmpass = (EditText) findViewById(R.id.confirm_password);
        register.setOnClickListener(this);
        gender.setOnClickListener(this);
        birthday.setOnClickListener(this);
    }

    void setdate() {
        final SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MM-yyyy");
        final Calendar newCalenda = Calendar.getInstance();
        DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                newCalenda.set(year, monthOfYear, dayOfMonth);
                String finaldate = (dateFormatter.format(newCalenda.getTime()));
                birthday.setText(finaldate);
            }
        }, newCalenda.get(Calendar.YEAR), newCalenda.get(Calendar.MONTH), newCalenda.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.getDatePicker().setMinDate(newCalenda.getTimeInMillis());
        datePickerDialog.show();
    }

    private void showDateDialog() {
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog dpd = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        // Display Selected date in textbox
                        birthday.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                    }
                }, mYear, mMonth, mDay);
        dpd.getDatePicker().setMaxDate(System.currentTimeMillis() - 10000);
        dpd.show();
    }

    public static Date getDateObject(String date, String date_format) {
        try {
            return new SimpleDateFormat(date_format).parse(date);
        } catch (ParseException e) {
            AppDelegate.LogE(e);
        }
        return Calendar.getInstance().getTime();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.birthday:
                //setdate();
                showDateDialog();
                break;
            case R.id.register:
                new Prefs(this).clearTempPrefs();
                gettext();
                validate();
                break;
        }
    }

    private void executeSignup() {
        try {
            if (AppDelegate.haveNetworkConnection(this, true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                PostAsync mPostAsyncObj;
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE.trim());
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.email, email_id.trim());
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.password, pass.trim());
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.gender, Gender.trim());
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.birthdate, birth.trim());
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.first_name, first_name.trim());
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.last_name, last_name.trim());
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.nick_name, nick_name.trim());
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.device_id, String.valueOf(AppDelegate.getUUID(this)).trim());
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.device_type, String.valueOf(2).trim());
                //  AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.device_token, AppDelegate.getValue(Signup.this, Tags.REGISTRATION_ID), ServerRequestConstants.Key_PostintValue);
                mPostAsyncObj = new PostAsync(this, this, ServerRequestConstants.REGISTER,
                        mPostArrayList, null);
                AppDelegate.showProgressDialog(this);
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
            AppDelegate.ShowDialog(this, "Please try again.", "Alert!!!");
        }
    }

    private void validate() {
        if (!AppDelegate.isValidString(first_name)) {
            AppDelegate.ShowDialog(this, getResources().getString(R.string.forname), "Alert!!!");
        } else if (!AppDelegate.isValidString(last_name)) {
            AppDelegate.ShowDialog(this, getResources().getString(R.string.forlastname), "Alert!!!");
        } else if (!AppDelegate.isValidString(nick_name)) {
            AppDelegate.ShowDialog(this, getResources().getString(R.string.fornick), "Alert!!!");
        }/* else if (!AppDelegate.isValidString(cityy)) {
            AppDelegate.ShowDialog(this, getResources().getString(R.string.forcity), "Alert!!!");
        }*/ else if (!AppDelegate.CheckEmail(email_id)) {
            AppDelegate.ShowDialog(this, getResources().getString(R.string.foremail), "Alert!!!");
        } else if (!AppDelegate.isValidString(Gender)) {
            AppDelegate.ShowDialog(this, getResources().getString(R.string.forgender), "Alert!!!");
        } else if (!AppDelegate.isValidString(birth)) {
            AppDelegate.ShowDialog(this, getResources().getString(R.string.forbirth), "Alert!!!");
        } else if (!AppDelegate.isValidString(pass)) {
            AppDelegate.ShowDialog(this, getResources().getString(R.string.forpass), "Alert!!!");
        } else if (pass.length() < 6) {
            AppDelegate.ShowDialog(this, getResources().getString(R.string.forlength), "Alert!!!");
        } else if (!AppDelegate.isValidString(confirm_pass)) {
            AppDelegate.ShowDialog(this, getResources().getString(R.string.forconpass), "Alert!!!");
        } else if (!pass.equals(confirm_pass)) {
            AppDelegate.ShowDialog(this, getResources().getString(R.string.formatch), "Alert!!!");
        } else if (!AppDelegate.password_validation(this, pass)) {

        } else {
            if (AppDelegate.haveNetworkConnection(this, false)) {
                executeSignup();
            } else {
                AppDelegate.ShowDialog(this, getResources().getString(R.string.not_connected), "Alert!!!");
            }
        }
    }

    private void gettext() {
        first_name = firstname.getText().toString();
        last_name = lastname.getText().toString();
        email_id = email.getText().toString();
        cityy = city.getText().toString();
        if (gender.isChecked()) {
            Gender = gender.getTextOn().toString();
        } else {
            Gender = gender.getTextOff().toString();
        }
        birth = birthday.getText().toString();
        pass = password.getText().toString();
        confirm_pass = confirmpass.getText().toString();
        nick_name = nickname.getText().toString();
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        AppDelegate.hideProgressDialog(this);
        if (!AppDelegate.isValidString(result)) {
            AppDelegate.ShowDialog(this, "Service Time Out!!!", "Time out!!!");
            return;
        }
        if (apiName.equals(ServerRequestConstants.REGISTER)) {
            parseRegister(result);
        }
    }

    private void parseRegister(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            UserDataModel userDataModel = new UserDataModel();
            userDataModel.message = jsonObject.getString(Tags.message);
            userDataModel.datatflow = jsonObject.getInt(Tags.dataFlow);
            if (jsonObject.getInt(Tags.status) == 1) {
                JSONObject object = jsonObject.getJSONObject(Tags.response);
                userDataModel = new UserDataModel();
                // userDataModel.status = object.getInt(Tags.status);
                userDataModel.first_name = object.getString(Tags.first_name);
                userDataModel.last_name = object.getString(Tags.last_name);
                userDataModel.email = object.getString(Tags.email);
                userDataModel.password = object.getString(Tags.password);
                userDataModel.created = object.getString(Tags.created);
                userDataModel.str_Gender = object.getString(Tags.gender);
                userDataModel.userId = object.getInt(Tags.user_id);
                userDataModel.dob = object.getString(Tags.birthdate);
                userDataModel.nickname = object.getString(Tags.nick_name);
                prefs.setUserData(userDataModel);
                AppDelegate.ShowDialogID(this, jsonObject.getString(Tags.message), "Alert", Tags.ok, Signup.this);
                } else {
                if (jsonObject.getInt(Tags.status) == 1) {
                    AppDelegate.ShowDialogID(this, jsonObject.getString(Tags.message), "Alert", "", Signup.this);
                } else if (jsonObject.has(Tags.response) && jsonObject.optJSONObject(Tags.response) != null /*&& jsonObject.getJSONObject(Tags.response).has(Tags.suspand_status) && jsonObject.getJSONObject(Tags.response).optString(Tags.suspand_status) != null*/) {
                    AppDelegate.showAlert(this, jsonObject.getString(Tags.message));
                }
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
            AppDelegate.showAlert(this, "Response is not proper. Please try again later.");
        }
    }
    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public void setOnDialogClickListener(String name) {
        if (name.equalsIgnoreCase(Tags.ok)) {
            Intent intent = new Intent(Signup.this, Question.class);
            startActivity(intent);
            finish();
        }
    }
}
