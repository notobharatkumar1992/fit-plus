package com.fitplus;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import Async.PostAsync;
import Constants.Parameters;
import Constants.ServerRequestConstants;
import Constants.Tags;
import adapters.FitDayAdapter;
import adapters.TrendingAdapter;
import interfaces.OnDialogClickListener;
import interfaces.OnReciveServerResponse;
import model.FitDayModel;
import model.PostAysnc_Model;
import model.UserDataModel;
import utils.Prefs;
import utils.SpacesItemDecoration;

public class ViewFitDay extends AppCompatActivity implements View.OnClickListener, OnReciveServerResponse, OnDialogClickListener {
    Activity mActivity;
    private RecyclerView trending_list;
    private Prefs prefs;
    private StaggeredGridLayoutManager gaggeredGridLayoutManager;
    private FitDayAdapter trending_Adapter;
    private Toolbar toolbar;
    private ImageView back;
    Intent intent;
    private int id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.trending_list);
        mActivity = this;
        findIDs();
        prefs = new Prefs(this);
        id=/*getIntent().getIntExtra("id",0);*/new Prefs(this).getIntValue("post_id",0);

        AppDelegate.LogT("user id"+id+"checked");
        execute_trendingList();
    }

    private void execute_trendingList() {
        try {
            if (AppDelegate.haveNetworkConnection(this, true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                PostAsync mPostAsyncObj;
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.user_id, id,ServerRequestConstants.Key_PostintValue);
                //  AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.device_token, AppDelegate.getValue(Signup.this, Tags.REGISTRATION_ID), ServerRequestConstants.Key_PostintValue);
                mPostAsyncObj = new PostAsync(this, this, ServerRequestConstants.VIEW_FITDAY,
                        mPostArrayList, null);
                AppDelegate.showProgressDialog(this);
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
            AppDelegate.ShowDialog(this, "Please try again.", "Alert!!!");
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mActivity != null) {
            mActivity = null;
        }
    }

    private void findIDs() {
        trending_list = (RecyclerView) findViewById(R.id.trending);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        back = (ImageView) toolbar.findViewById(R.id.back);
        back.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back:
                finish();
                break;
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        AppDelegate.hideProgressDialog(this);
        if (!AppDelegate.isValidString(result)) {
            AppDelegate.ShowDialog(this, getResources().getString(R.string.time_out), "Time out!!!");
            return;
        }
        if (apiName.equals(ServerRequestConstants.VIEW_FITDAY)) {
            new Prefs(this).clearTempPrefs();
            parseTrendingList(result);
        }
    }

    private void parseTrendingList(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getInt(Tags.status) == 1) {
                JSONArray jsonArray = jsonObject.getJSONArray(Tags.response);
                ArrayList<FitDayModel> trending = new ArrayList<>();
                for (int i = 0; i < jsonArray.length(); i++) {
                    FitDayModel userDataModel = new FitDayModel();
                    JSONObject object = jsonArray.getJSONObject(i);
                    userDataModel.id= object.getInt(Tags.user_id);
                    userDataModel.file_name = object.getString(Tags.file_name);
                    userDataModel.file_type = object.getInt(Tags.file_type);
                    userDataModel.user_id = object.getInt(Parameters.user_id);
                    userDataModel.view = object.getInt(Tags.view);
                    userDataModel.comment = object.getString(Tags.comment);
                    userDataModel.created = object.getString(Tags.created);
                    userDataModel.file_thumb = object.getString(Tags.file_thumb);
                    trending.add(userDataModel);
                }
                setTrendingList(trending);

            } else {
                if(jsonObject.getInt(Tags.status) == 0){
                    AppDelegate.ShowDialogID(this, jsonObject.getString(Tags.message), "Alert", Tags.ok, ViewFitDay.this);
                }
               else if (jsonObject.has(Tags.response) && jsonObject.optJSONObject(Tags.response) != null /*&& jsonObject.getJSONObject(Tags.response).has(Tags.suspand_status) && jsonObject.getJSONObject(Tags.response).optString(Tags.suspand_status) != null*/) {
                    AppDelegate.ShowDialogID(this, jsonObject.getString(Tags.message), "Alert", Tags.ok, ViewFitDay.this);
                }
            }
        } catch (Exception e) {
            AppDelegate.showAlert(this, getResources().getString(R.string.response_error));
            AppDelegate.LogE(e);
        }

    }

    private void setTrendingList(ArrayList<FitDayModel> trending) {
        trending_list.setPadding(AppDelegate.dpToPix(this, 5), AppDelegate.dpToPix(this, 5), AppDelegate.dpToPix(this, 5), AppDelegate.dpToPix(this, 5));
        trending_list.setHasFixedSize(true);
        gaggeredGridLayoutManager = new StaggeredGridLayoutManager(2, 1);
        trending_list.setLayoutManager(gaggeredGridLayoutManager);
        trending_list.addItemDecoration(new SpacesItemDecoration(AppDelegate.dpToPix(this, 5)));
        trending_Adapter = new FitDayAdapter(this, trending);
        trending_list.setAdapter(trending_Adapter);
    }

    @Override
    public void setOnDialogClickListener(String name) {

    }
}
