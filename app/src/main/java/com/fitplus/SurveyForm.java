package com.fitplus;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextSwitcher;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;

import org.json.JSONObject;

import java.util.ArrayList;

import Async.PostAsync;
import Constants.Parameters;
import Constants.ServerRequestConstants;
import Constants.Tags;
import interfaces.OnDialogClickListener;
import interfaces.OnReciveServerResponse;
import io.fabric.sdk.android.Fabric;
import model.BecomeCoachModel;
import model.PostAysnc_Model;
import model.UserDataModel;
import utils.Prefs;

/**
 * Created by admin on 26-07-2016.
 */
public class SurveyForm extends AppCompatActivity implements OnClickListener, OnReciveServerResponse, OnDialogClickListener {
    private LinearLayout ll1, ll2, ll3;
    private String first, second, third;
    private RadioGroup options;
    private TextView questions, next, prev, question_number;
    private RadioButton selected_option,option1,option2,option3,option4;
    private Activity mActivity;
    private Bundle bundle;
    private int index=0;
    private ArrayList<BecomeCoachModel> become_coach;
    ArrayList<String> answer_list=new ArrayList<>();
    private int id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.survay_form);
        bundle=getIntent().getExtras();
        become_coach=bundle.getParcelableArrayList(Tags.parcel_becomeCoach);
        mActivity = null;
        initView();
    }

    private void initView() {
        questions = (TextView) findViewById(R.id.question);
        TextSwitcher textSwitcher = new TextSwitcher(this);
        textSwitcher.setInAnimation(this, R.anim.slide_in_left);
        textSwitcher.setOutAnimation(this, R.anim.slide_out_right);
        textSwitcher.addView(questions);
        textSwitcher.addView(questions);
        RelativeLayout relative=(RelativeLayout)findViewById(R.id.relative);
        relative.addView(textSwitcher);
        next = (TextView) findViewById(R.id.next);
        prev = (TextView) findViewById(R.id.prev);
        question_number = (TextView) findViewById(R.id.question_number);
        options = (RadioGroup) findViewById(R.id.options);
        option1 = (RadioButton) findViewById(R.id.option1);
        option2 = (RadioButton) findViewById(R.id.option2);
        option3 = (RadioButton) findViewById(R.id.option3);
        option4 = (RadioButton) findViewById(R.id.option4);
        questions.setText(become_coach.get(0).question);
        option1.setText(become_coach.get(0).option1);
        option2.setText(become_coach.get(0).option2);
        option3.setText(become_coach.get(0).option3);
        option4.setText(become_coach.get(0).option4);
    }

    @Override
    public void onBackPressed() {

    }

    private void changeVisibility(int value) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.options:
               id= options.getCheckedRadioButtonId();
                break;
            case R.id.next:
                selected_option=(RadioButton)findViewById(id);
                answer_list.add(  selected_option.getText().toString().trim());
                become_coach.get(index).answer_status=1;
                executeNext();
                break;
            case R.id.prev:

                break;
        }
    }

    private void executeNext() {
        index++;
        questions.setText(become_coach.get(index).question);
        option1.setText(become_coach.get(index).option1);
        option2.setText(become_coach.get(index).option2);
        option3.setText(become_coach.get(index).option3);
        option4.setText(become_coach.get(index).option4);
    }

    private void execute_UserCategory() {
        try {
            if (AppDelegate.haveNetworkConnection(this, true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                PostAsync mPostAsyncObj;
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE.trim());
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.user_id, new Prefs(this).getUserdata().userId);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.category_one, first.trim());
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.category_two, second.trim());
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.category_three, third.trim());
                //  AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.device_token, AppDelegate.getValue(Signup.getActivity(), Tags.REGISTRATION_ID), ServerRequestConstants.Key_PostintValue);
                mPostAsyncObj = new PostAsync(this, SurveyForm.this, ServerRequestConstants.USER_CATEGORY, mPostArrayList, null);
                AppDelegate.showProgressDialog(this);
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
            AppDelegate.ShowDialog(this, "Please try again.", "Alert!!!");
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        AppDelegate.hideProgressDialog(this);
        if (!AppDelegate.isValidString(result)) {
            AppDelegate.ShowDialog(this, this.getResources().getString(R.string.time_out), "Time out!!!");
            return;
        }
        if (apiName.equals(ServerRequestConstants.USER_CATEGORY)) {
            parseCategory(result);
        }
    }

    private void parseCategory(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getInt(Tags.status) == 1) {
                JSONObject object = jsonObject.getJSONObject(Tags.response);
                UserDataModel userDataModel = new UserDataModel();
                userDataModel.first_name = object.getString(Tags.first_name);
                userDataModel.last_name = object.getString(Tags.last_name);
                userDataModel.email = object.getString(Tags.email);
                userDataModel.fat_status = object.getString(Tags.fat_status);
                userDataModel.password = object.getString(Tags.password);
                userDataModel.created = object.getString(Tags.created);
                userDataModel.str_Gender = object.getString(Tags.gender);
                userDataModel.userId = object.getInt(Tags.user_id);
                userDataModel.dob = object.getString(Tags.birthdate);
                userDataModel.nickname = object.getString(Tags.nick_name);
                userDataModel.avtar = object.getString(Tags.avtar);
                userDataModel.views = object.getString(Tags.views);
                userDataModel.follower_count = object.getInt(Tags.follower_count);
                userDataModel.profile_visibility = object.getInt(Tags.profile_visibility);
                userDataModel.presentation_vedio = object.getString(Tags.presentation_vedio);
                userDataModel.bank_account = object.getString(Tags.bank_account);
                userDataModel.certificates = object.getString(Tags.certificates);
                userDataModel.country_id = object.getString(Tags.country_id);
                userDataModel.role_id = object.getInt(Tags.role_id);
                userDataModel.state_id = object.getString(Tags.state_id);
                userDataModel.city_id = object.getString(Tags.city_id);
                userDataModel.post_code = object.getString(Tags.post_code);
                userDataModel.sex_group = object.getString(Tags.sex_group);
                userDataModel.type = object.getString(Tags.type);
                userDataModel.token = object.getString(Tags.token);
                userDataModel.is_login = object.getInt(Tags.is_login);
                userDataModel.is_social = object.getInt(Tags.is_social);
                userDataModel.is_verified = object.getInt(Tags.is_verified);
                userDataModel.latitude = object.getDouble(Tags.latitude);
                userDataModel.longitude = object.getDouble(Tags.longitude);
                new Prefs(this).setUserData(userDataModel);
                AppDelegate.ShowDialogID(this, jsonObject.getString(Tags.message), "Alert", Tags.ok, SurveyForm.this);
            } else {
                if (jsonObject.has(Tags.response) && jsonObject.optJSONObject(Tags.response) != null /*&& jsonObject.getJSONObject(Tags.response).has(Tags.suspand_status) && jsonObject.getJSONObject(Tags.response).optString(Tags.suspand_status) != null*/) {
                    AppDelegate.showAlert(this, jsonObject.getString(Tags.message));
                }
            }
        } catch (Exception e) {
            AppDelegate.showAlert(this, getResources().getString(R.string.response_error));
            AppDelegate.LogE(e);
        }
    }


    @Override
    public void setOnDialogClickListener(String name) {
        if (name.equalsIgnoreCase(Tags.ok)) {
            startActivity(new Intent(SurveyForm.this, MainActivity.class));
            finish();
        }
    }
}
