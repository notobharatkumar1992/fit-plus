package com.fitplus;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;
import utils.Prefs;

public class Splash extends AppCompatActivity {
    private Intent mainIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.splash);
        final Prefs prefs = new Prefs(this);
        //   if (prefs.getUserdata() != null && AppDelegate.isValidString(prefs.getUserId())){

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (prefs.getUserdata() != null && AppDelegate.isValidString(String.valueOf(prefs.getUserdata().userId))) {
                    if (!AppDelegate.isValidString(String.valueOf(prefs.getUserdata().fat_status))) {
                        startActivity(new Intent(Splash.this, Question.class));
                    } else {
                        startActivity(new Intent(Splash.this, MainActivity.class));
                    }
                    finish();
                } else {
                    startActivity(new Intent(Splash.this, GetStart.class));
                    finish();
                }
            }
        }, 2000);
    }

    @Override
    public void onBackPressed() {
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
