package com.fitplus;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import Async.PostAsync;
import Constants.Parameters;
import Constants.ServerRequestConstants;
import Constants.Tags;
import interfaces.OnDialogClickListener;
import interfaces.OnReciveServerResponse;
import model.PostAysnc_Model;
import utils.Prefs;
import utils.ProgressD;

public class ResetPassword extends AppCompatActivity implements View.OnClickListener, OnReciveServerResponse, OnDialogClickListener {
    private Intent intent;
    private String Email;
    EditText password, confirm_password;
    TextView submit;
    private boolean result;
    public static ResetPassword forgotpassword;
    private Toolbar toolbar;
    private String identifier;
    private int status;
    int varificationcode = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reset_password);
        forgotpassword = this;
        set_toolbar();
        initializeObjects();
    }

    private void set_toolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (forgotpassword != null)
                    forgotpassword.finish();
            }
        });
    }

    private void initializeObjects() {
        password = (EditText) findViewById(R.id.new_password);
        confirm_password = (EditText) findViewById(R.id.confirm_password);
        submit = (TextView) findViewById(R.id.submit);
        submit.setOnClickListener(this);
        identifier = AppDelegate.getValue(this, Tags.IDENTIFIER);
    }

    private void validate() {
        if (!AppDelegate.isValidString(password.getText().toString())) {
            AppDelegate.ShowDialog(this, getResources().getString(R.string.forpass), "Alert!!!");
        } else if (password.getText().toString().length() < 6) {
            AppDelegate.ShowDialog(this, getResources().getString(R.string.forlength), "Alert!!!");
        } else if (!AppDelegate.isValidString(confirm_password.getText().toString())) {
            AppDelegate.ShowDialog(this, getResources().getString(R.string.forconpass), "Alert!!!");
        } else if (!password.getText().toString().equals(confirm_password.getText().toString())) {
            AppDelegate.ShowDialog(this, getResources().getString(R.string.formatch), "Alert!!!");
        } else if (!AppDelegate.password_validation(this, password.getText().toString())) {

        } else {
            if (AppDelegate.haveNetworkConnection(this, false)) {
                executereset();
            } else {
                AppDelegate.ShowDialog(this, getResources().getString(R.string.not_connected), "Alert!!!");
            }
        }

    }

    private void executereset() {
        try {
            if (AppDelegate.haveNetworkConnection(this, true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                PostAsync mPostAsyncObj;
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE.trim());
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.user_id, new Prefs(this).getUserdata().userId);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.password, password.getText().toString().trim());
                //  AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.device_token, AppDelegate.getValue(Signup.this, Tags.REGISTRATION_ID), ServerRequestConstants.Key_PostintValue);
                mPostAsyncObj = new PostAsync(this, this, ServerRequestConstants.RESET_PASSWORD,
                        mPostArrayList, null);
                AppDelegate.showProgressDialog(this);
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
            AppDelegate.ShowDialog(this, "Please try again.", "Alert!!!");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppDelegate.hideKeyBoard(this);
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    protected void onDestroy() {
        if (forgotpassword != null) {
            forgotpassword = null;
        }
        ProgressD.hideProgress_dialog();
        System.gc();
        super.onDestroy();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.submit:
                validate();
        }
    }


    @Override
    public void setOnReciveResult(String apiName, String result) {
        AppDelegate.hideProgressDialog(this);
        if (!AppDelegate.isValidString(result)) {
            AppDelegate.ShowDialog(this, "Service Time Out!!!", "Time out!!!");
            return;
        }
        if (apiName.equals(ServerRequestConstants.RESET_PASSWORD)) {
            parseReset(result);
        }
    }

    private void parseReset(String result) {
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(result);
            status = jsonObject.getInt("status");
            String message = jsonObject.getString("message");
            AppDelegate.ShowDialogID(ResetPassword.this, message + "", "Alert!!!", "ForgotPassword", this);
            if (status == 1) {
                JSONObject jsonArray = jsonObject.getJSONObject(Tags.response);

            } else {
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setOnDialogClickListener(String name) {
        if (name.equals("ForgotPassword")) {
            if (status == 1) {
                finish();
            } else {
                password.setText("");
                confirm_password.setText("");
            }
        }
    }
}
