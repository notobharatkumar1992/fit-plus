package com.fitplus;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import Async.PostAsync;
import Constants.Parameters;
import Constants.ServerRequestConstants;
import Constants.Tags;
import interfaces.OnDialogClickListener;
import interfaces.OnReciveServerResponse;
import model.FitDayModel;
import model.PostAysnc_Model;

public class PlayVideo extends AppCompatActivity implements OnReciveServerResponse, OnDialogClickListener {

Boolean show=false;
    private VideoView videoView;
    public static Activity activity;
    private String identifier;
    private String url;
    private Bundle bundle;
    FitDayModel fitday;
    private TextView comment;
    private MediaController mMediaController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.play_video);
        identifier = AppDelegate.getValue(this, Tags.IDENTIFIER);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        findIDS();

        activity = this;
        bundle = getIntent().getExtras();
        fitday = bundle.getParcelable("FitDay");
        if(fitday!=null) {
            comment = (TextView) findViewById(R.id.comment);
            comment.setText(fitday.comment + "");
            playvdo(fitday.file_name);
        }
    }
    private void findIDS() {
        videoView = (VideoView) findViewById(R.id.video);
    }
    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (activity != null) {
            activity = null;
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        AppDelegate.hideProgressDialog(this);
        if (!AppDelegate.isValidString(result)) {
            AppDelegate.ShowDialog(this, "Service Time Out!!!", "Time out!!!");
            return;
        }
    }
    private void playvdo(String url) {

        final ProgressDialog pd=new ProgressDialog(this);
        pd.requestWindowFeature(Window.FEATURE_NO_TITLE);
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setMessage("Loading...");
        pd.setCancelable(false);
        pd.show();
        mMediaController = new MediaController(this);
        videoView.setMediaController(mMediaController);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(show==false){
                    pd.dismiss();
                   // AppDelegate.showDialog_okCancel(PlayVideo.this, "Service Time Out!!!", "next", "retry", PlayVideo.this);

                }
            }
    }, 60000);
         videoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
             @Override
             public boolean onError(MediaPlayer mp, int what, int extra) {
                 Log.d("video", "setOnErrorListener ");
                 pd.dismiss();
                 AppDelegate.ShowDialogID(PlayVideo.this, "Can't play this vedio", "Alert", "ERROR", PlayVideo.this);
                 return true;
             }
         });
         videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
             @Override
             public void onPrepared(MediaPlayer mp) {
                 show = true;
                 pd.dismiss();
                 mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                     @Override
                     public void onCompletion(MediaPlayer mp) {

                     }
                 });
             }
         });
         videoView.setVideoURI(Uri.parse(url));
         videoView.requestFocus();
         videoView.start();
     }
     @Override
     public void setOnDialogClickListener(String name) {
         if (name.equalsIgnoreCase("ERROR")) {

         }else if (name.equalsIgnoreCase("next")) {
             finish();
         } else if (name.equalsIgnoreCase("retry")) {
             playvdo(url);
         }
     }
 }
