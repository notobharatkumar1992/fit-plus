package adapters;


import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.fitplus.AppDelegate;
import com.fitplus.R;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import model.UserDataModel;
import staggeredView.DataSet;
import staggeredView.STGVImageView;


public class TrendingAdapter extends RecyclerView.Adapter<TrendingAdapter.ViewHolder> {
   private final ArrayList<UserDataModel> trending_list;
    View v;
DataSet ds;
    FragmentActivity context;
    private LayoutInflater mInflater;
    private Map map = new HashMap<>();
    private Map immap = new HashMap<>();
    ArrayList<String> images = new ArrayList<>();
    private Bundle bundle;
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.trending_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final UserDataModel userDataModel = trending_list.get(position);
       holder.text.setText(userDataModel.nickname);
        Picasso.with(context).load(userDataModel.avtar_thumb ).into(new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                holder.image.mWidth = bitmap.getWidth();
                holder.image.mHeight = bitmap.getHeight();
                holder.image.setImageBitmap(bitmap);
            }
            @Override
            public void onBitmapFailed(Drawable errorDrawable) {
            }
            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {
            }
        });
    }
    public TrendingAdapter(FragmentActivity context, ArrayList<UserDataModel> trending_list) {
        this.context = context;
       this.trending_list = trending_list;
      // this. ds=ds;
    }
    @Override
    public int getItemCount() {
        return trending_list.size();
    }
    class ViewHolder extends RecyclerView.ViewHolder {
        TextView text;
        ImageView star,dots;
        STGVImageView image;

        public ViewHolder(View itemView) {
            super(itemView);
            text = (TextView) itemView.findViewById(R.id.desc);
            image = (STGVImageView) itemView.findViewById(R.id.img_content);
            star = (ImageView) itemView.findViewById(R.id.star);
            dots = (ImageView) itemView.findViewById(R.id.dots);
        }
    }
}