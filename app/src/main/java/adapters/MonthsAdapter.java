package adapters;


import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fitplus.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class MonthsAdapter extends RecyclerView.Adapter<MonthsAdapter.ViewHolder> {
    View v;
    String[] title;
    FragmentActivity context;
    private LayoutInflater mInflater;
    private Map map = new HashMap<>();
    private Map immap = new HashMap<>();
    ArrayList<String> images = new ArrayList<>();
    private Bundle bundle;
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.months_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        // bundle=new Bundle();
        //  final PointsHistoryModel dv = pointsHistory.get(position);
        holder.month.setText(title[position]);
        holder.month.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for(int i=0;i<title.length;i++){
                    holder.month.setTextColor(context.getResources().getColor(R.color.stats_textcolor));
                    holder.bar.setBackgroundResource(0);
                }
                holder.month.setTextColor(context.getResources().getColor(R.color.white));
                holder.bar.setBackgroundResource(R.color.orange);
            }
        });
    }
    public MonthsAdapter(FragmentActivity context, String[] title) {
        this.context = context;
        this.title = title;
    }
    @Override
    public int getItemCount() {
        return title.length;
    }
    class ViewHolder extends RecyclerView.ViewHolder {
        TextView month;
        LinearLayout bar;

        public ViewHolder(View itemView) {
            super(itemView);
            month = (TextView) itemView.findViewById(R.id.month);
            bar = (LinearLayout) itemView.findViewById(R.id.bar);
        }
    }
}